import React from 'react';
import {createBrowserHistory} from 'history';
import ReactDOM from 'react-dom';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import  Home  from './components/Home';
import Matakuliah from './components/Diskusi/Matakuliah';
import Pertanyaan from './components/Diskusi/Pertanyaan';
import Jawaban from './components/Diskusi/Jawaban';
import Nav from './components/Nav'
import Kurikulum from './components/Kurikulum';
import KurikulumWajib from './components/Kurikulum/KurikulumWajib';
import KurikulumPilihan from './components/Kurikulum/KurikulumPilihan';


import '../resources/scss/style.scss';

class App extends React.Component{
    render() {
        return (
            <div>
            <BrowserRouter history={createBrowserHistory}>
            <Nav/>

                <Switch>
                    <Route exact path={"/"} component={Home} />
                    <Route exact path={"/kurikulum"} component={Kurikulum} />
                    <Route path={"/kurikulum/wajib/182/:tahun_kurikulum"} component={KurikulumWajib} />
                    <Route path={"/kurikulum/pilihan/182/:tahun_kurikulum"} component={KurikulumPilihan} />
                    <Route exact path={"/thread"} component={Matakuliah} />
                    <Route path={"/thread/:kode_matkul"} component={Pertanyaan} />
                    <Route path={"/jawaban/:id_thread"} component={Jawaban} />
                </Switch>
                    
    </BrowserRouter>
    </div>
        );
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root'),
);