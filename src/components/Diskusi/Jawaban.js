import React from 'react';
import Diskusi from '../Template/Diskusi'
import Thread from '../Template/Thread'
import { Input, FormGroup, Label, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';


class Jawaban extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            jawaban: [],
            thread: [],
            jawabanBaru: {
                id_jawaban:'',
                id_thread: this.props.match.params.id_thread,
                jawaban:'',
                favorit:0,
                userid:'',
            },
            newJawabanModal: false
        };
        
    }
    
    componentDidMount(){
        const {id_thread} = this.props.match.params;
        fetch(`http://178.128.104.74:6010/api/v2/jawaban/${id_thread}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        jawaban: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
        fetch(`http://178.128.104.74:6010/api/v2/thread/one/${id_thread}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        thread: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    toggleNewJawabanModal() {
        this.setState({
          newJawabanModal: ! this.state.newJawabanModal
        });
    }
    addJawaban() {
        fetch('http://178.128.104.74:6010/api/v2/jawaban', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(this.state.jawabanBaru), // Baru can be `string` or {object}!
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });

        //.then((response) => {
        //    console.log(response);
        

        this.setState({ newJawabanModal: false, jawabanBaru: {
            id_jawaban:'',
            id_thread: this.props.match.params.id_thread,
            jawaban:'',
            favorit:0,
            userid:'',
        }});
    }

    render() {
        let jawaban = this.state.jawaban.map((jawab) => {
            return(
                <ul>                
                    <div key={jawab.id_jawaban}>
                        <Diskusi konten={jawab.jawaban} favorit={jawab.favorit} />
                    </div>
                </ul>
            )
        });

        let thread = this.state.thread.map((pertanyaan) => {
            return(
                <ul>                
                    <div>
                        <Thread judul={pertanyaan.judul_thread} konten={pertanyaan.pertanyaan} link="#" favorit={pertanyaan.favorit} isdone={pertanyaan.isdone ? 'Diselesaikan' : 'Belum Selesai'} />              
                    </div>
                </ul>
            )
        });

        return(
            <div>
                <div className="row">
                </div>
                <Button className="my-3" color="success" onClick={this.toggleNewJawabanModal.bind(this)}>Add Jawaban</Button>
                <Modal isOpen={this.state.newJawabanModal} toggle={this.toggleNewJawabanModal.bind(this)}>
                    <ModalHeader toggle={this.toggleNewJawabanModal.bind(this)}>Add a new jawaban</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="id_jawaban">ID Jawaban</Label>
                            <Input id="id_jawaban" value={this.state.jawabanBaru.id_jawaban} onChange={(e) => {
                            let { jawabanBaru } = this.state;

                            jawabanBaru.id_jawaban = parseInt(e.target.value);

                            this.setState({ jawabanBaru });
                            }} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="jawaban">Jawaban</Label>
                            <Input id="jawaban" value={this.state.jawabanBaru.jawaban} onChange={(e) => {
                            let { jawabanBaru } = this.state;

                            jawabanBaru.jawaban = e.target.value;

                            this.setState({ jawabanBaru });
                            }} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="userid">user id</Label>
                            <Input id="userid" value={this.state.jawabanBaru.userid} onChange={(e) => {
                            let { jawabanBaru } = this.state;

                            jawabanBaru.userid = e.target.value;

                            this.setState({ jawabanBaru });
                            }} />
                        </FormGroup>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.addJawaban.bind(this)}>Add Jawaban</Button>{' '}
                        <Button color="secondary" onClick={this.toggleNewJawabanModal.bind(this)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            
                {thread};
                {jawaban};
            </div>
        )
    }
}
export default Jawaban;