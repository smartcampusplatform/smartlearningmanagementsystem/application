import React from 'react';
import ButtonMenu from '../Template/ButtonMenu'
import { faHome } from '@fortawesome/free-solid-svg-icons'

class Matakuliah extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            matkul: []
        };
    }
    
    componentDidMount(){
        fetch('http://178.128.104.74:6010/api/v2/matakuliah')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        matkul: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    
    render() {
        const { error, isLoaded, matkul} =
        this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded){
            return <div>Loading...</div>;
        } else {
            return ( 
                <ul>    
                    <div className=" row justify-content-lg-center" style={{marginTop : '50px'}}>
                        {matkul.map(matakuliah => (
                            <div key={matakuliah.id_matkul}>
                                <ButtonMenu name={matakuliah.kode_matkul} icon={faHome} link={"/thread/"+matakuliah.kode_matkul} />     
                                {matakuliah.nama_matkul}
                            </div>
                        ))}
                    </div>
                    Things to do : 
                    <li>
                        Search Thread
                    </li>
                    <li>
                        Nambah favorit dalam thread dan jawaban
                    </li>
                </ul>
            );
        }
    }
}
export default Matakuliah;