import React from 'react';
import Thread from '../Template/Thread'
import { Input, FormGroup, Label, Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

class Pertanyaan extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            pertanyaan: [],
            threadBaru: {
                id_thread:'',
                kode_matkul: this.props.match.params.kode_matkul,
                judul_thread:'',
                pertanyaan:'',
                isdone: false,
                favorit: 0,
                userid:'',
            },
            newThreadModal: false
        };
    }
    
    componentDidMount(){
        const {kode_matkul} = this.props.match.params;
        fetch(`http://178.128.104.74:6010/api/v2/thread/${kode_matkul}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        pertanyaan: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
        
    }
    toggleNewThreadModal() {
        this.setState({
          newThreadModal: ! this.state.newThreadModal
        });
      }

    componentDidUpdate(prevProps){
        if (this.props.match.params !== prevProps.match.params){
            const {kode_matkul} = this.props.match.params;
            this.fetchData(kode_matkul);
        }
    }
    
    addThread() {
        var a = JSON.stringify(this.state.threadBaru)
        console.log(a)
        fetch('http://178.128.104.74:6010/api/v2/thread', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(this.state.threadBaru), // Baru can be `string` or {object}!
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });

        //.then((response) => {
        //    console.log(response);
        

        this.setState({ newThreadModal: false, threadBaru: {
            id_thread:'',
            kode_matkul: this.props.match.params.kode_matkul,
            judul_thread:'',
            pertanyaan:'',
            isdone: false,
            favorit: 0,
            userid:'',
        }});
    }

    render() {
        let pertanyaan = this.state.pertanyaan.map((thread) => {
            return(
                <ul>                
                    <div key={thread.id_thread}>
                        <Thread judul={thread.judul_thread} konten={thread.pertanyaan} link={"/jawaban/"+thread.id_thread} favorit={thread.favorit} isdone={thread.isdone ? 'Diselesaikan' : 'Belum Selesai'} />     
                    </div>
                </ul>
            )
        });

        return(
            <div>
                <div className="row">
                </div>
                <Button className="my-3" color="success" onClick={this.toggleNewThreadModal.bind(this)}>Add Thread</Button>
                <Modal isOpen={this.state.newThreadModal} toggle={this.toggleNewThreadModal.bind(this)}>
                    <ModalHeader toggle={this.toggleNewThreadModal.bind(this)}>Add a new thread</ModalHeader>
                    <ModalBody>
                    <FormGroup>
                        <Label for="id_thread">ID Thread</Label>
                        <Input id="id_thread" value={this.state.threadBaru.id_thread} onChange={(e) => {
                        let { threadBaru } = this.state;

                        threadBaru.id_thread = parseInt(e.target.value);

                        this.setState({ threadBaru });
                        }} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="judul_thread">Judul Thread</Label>
                        <Input id="judul_thread" value={this.state.threadBaru.judul} onChange={(e) => {
                        let { threadBaru } = this.state;

                        threadBaru.judul_thread = e.target.value;

                        this.setState({ threadBaru });
                        }} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="pertanyaan">Pertanyaan</Label>
                        <Input id="pertanyaan" value={this.state.threadBaru.pertanyaan} onChange={(e) => {
                        let { threadBaru } = this.state;

                        threadBaru.pertanyaan = e.target.value;

                        this.setState({ threadBaru });
                        }} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="userid">user id</Label>
                        <Input id="userid" value={this.state.threadBaru.userid} onChange={(e) => {
                        let { threadBaru } = this.state;

                        threadBaru.userid = parseInt(e.target.value);

                        this.setState({ threadBaru });
                        }} />
                    </FormGroup>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={this.addThread.bind(this)}>Add Thread</Button>{' '}
                        <Button color="secondary" onClick={this.toggleNewThreadModal.bind(this)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            
                {pertanyaan};
            </div>
        )
    }
}
export default Pertanyaan;