import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faNewspaper } from '@fortawesome/free-solid-svg-icons'


class Home extends React.Component {
    render() {
        return (
            
                    <div className="col-12 mt-5 ">
                        <h4 className="text-center">Selamat datang, silahkan pilih layanan yang tersedia dengan menekan tombol </h4>
                        <div className="row pt-3 justify-content-md-center">
                            <div className="col col-2">
                                <FontAwesomeIcon icon={faNewspaper} size="10x"/>
                            </div>
                            <div className="col col-6">
                            <p>Smart Learning Management System (SLMS) adalah sistem online yang menyediakan manajemen kurikulum, silabus, SAP, materi, kelas, ekivalensi, dan diskusi.</p>
                            <p>Created by: Kelompok 1</p>
                            <p>Eliezer Christanto ( 18213020 )
                            Reyhan Tanuwijaya ( 18216009 )<br/>
                            Muhammad Fata Nurrahman ( 18216014 )
                            Luthfi Fachriza Sandi ( 18216027 )
                            </p>
                            </div>
                        </div>
                    </div>
                
            
           );
    }


}

export default Home;