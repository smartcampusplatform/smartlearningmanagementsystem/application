import React from 'react';
import { faClipboardList, faUsers, faHome } from '@fortawesome/free-solid-svg-icons'
import ButtonMenu from './Button/ButtonMenu'

class Menu extends React.Component {
    render() {
        return (
      <div className="row justify-content-md-center">
            <ButtonMenu name="Home" icon={faHome} link="/" buttonclass="btn btn-primary p-2" />      
            <ButtonMenu name="Kurikulum" icon={faClipboardList} link="/kurikulum" buttonclass="btn btn-primary p-2"/>
            <ButtonMenu name="Kelas" icon={faUsers} link="/kelas" buttonclass="btn btn-primary p-2"/>
            <ButtonMenu name="Thread" icon={faUsers} link="/thread" buttonclass="btn btn-primary p-2"/>
            
        </div>    
           );
    }


}

export default Menu;
