import React from 'react';
import {Link } from 'react-router-dom';

class ButtonNormal extends React.Component{
    render(){
        return(
            <div className={this.props.col}>
                <Link to={this.props.link} className={this.props.buttonclass} style={{minWidth: '150px' , height: '40px'}}>
                <span>{this.props.name}</span></Link>
            </div>   
        );
    }
}

export default ButtonNormal;