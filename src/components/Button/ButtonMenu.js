import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {Link } from 'react-router-dom';

class ButtonMenu extends React.Component{
    render(){
        return(
            <div className="mt-5 col-4 text-center">
                <Link to={this.props.link} className={this.props.buttonclass} style={{minWidth: '150px' , height: '40px'}}>
                <FontAwesomeIcon  icon={this.props.icon} />
                <span className="ml-3">{this.props.name}</span></Link>
            </div>   
        );
    }
}

export default ButtonMenu;