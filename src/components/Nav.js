import React from 'react';
import Menu from './Menu';

class Nav extends React.Component {
    render() {
        return (
            <div className="container">
               
                <div className="row pt-5">
                    <div className="col-12 mb-2 ">
                        <h1 className="text-center">Smart Learning Management System</h1>
                    </div>
                </div>
                    <Menu />
               
            </div>
            
           );
    }


}

export default Nav;