import React from 'react';
import ButtonNormal from '../Button/ButtonNormal';
class Kurikulum extends React.Component {

    render() {
            return (
                <div className="col-12 mt-5 ">
                <h4 className="text-center">Silahkan pilih kurikulum yang tersedia dengan menekan tombol </h4>
                <div className="row justify-content-md-center">
            <ButtonNormal name="2013" icon={null} col="col-4 text-center" link="/kurikulum/wajib/182/2013" buttonclass="btn btn-secondary p-2" />      
            <ButtonNormal name="2008" icon={null} col="col-4 text-center" link="/kurikulum/wajib/182/2008" buttonclass="btn btn-secondary p-2"/>
        </div>
       
            </div>
            
                );



        }
    }




export default Kurikulum;