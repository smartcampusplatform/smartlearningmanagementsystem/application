import React from 'react';
import Kurikulum from '../../Kurikulum';
import ButtonNormal from '../../Button/ButtonNormal';

class KurikulumPilihan extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            kurikulum: []
        };
    }

    fetchData(tahun){
        fetch(`http://178.128.104.74:6010/api/v2/kurikulum/pilihan/182/${tahun}`)
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    kurikulum: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }
    
    componentDidMount(){
        const {tahun_kurikulum} = this.props.match.params;
        this.fetchData(tahun_kurikulum);
    }
    componentDidUpdate(prevProps){
        if (this.props.match.params !== prevProps.match.params){
            const {tahun_kurikulum} = this.props.match.params;
            this.fetchData(tahun_kurikulum);
        }
    }


    
    render() {
        const { error, isLoaded, kurikulum} =
        this.state;
        if (error) {
            return (
           
            <div>
                 <Kurikulum/>
                <div>Error: {error.message}</div>
            </div>
            )
        } else if (!isLoaded){
            return (
            
            <div>
                <Kurikulum/>
                <div>Loading...</div>
                
            </div>);
        } else {
            var kurwajib = "/kurikulum/wajib/182/" + this.props.match.params.tahun_kurikulum;
            var kurpilihan = "/kurikulum/pilihan/182/" + this.props.match.params.tahun_kurikulum;
            return(
            <div>
                <Kurikulum/>
                <div className="row">
            <ButtonNormal name="Wajib" col="col-2 text-center my-3" icon={null} link={kurwajib} buttonclass="btn btn-outline-dark p-2" />      
            <ButtonNormal name="Pilihan" col="col-2 text-center my-3" icon={null} link={kurpilihan} buttonclass="btn btn-outline-dark p-2"/>
        </div>
                <div>
              
                <div className="row mx-5">
                <div className="col">
                <h5>Matakuliah Pilihan</h5>
                <table className="table table-bordered mt-2">
            <thead className="table-dark">
                <tr >
                <td>No</td>
                <td>Kode Mata Kuliah</td>
                <td>Nama Mata Kuliah</td>
                <td>SKS</td>
                </tr>
            </thead>
            <tbody>
            {kurikulum.map((matkul, i) => (
                <tr key={matkul.id_kurikulum}>
                     <th scope="row">{i+1}</th>
                    <td>{matkul.kode_matkul}</td>
                    <td>{matkul.nama_matkul}</td>
                    <td>{matkul.sks}</td>
                   
                </tr>
            ))}
            </tbody>
        </table>
        </div>
        <div className="col">

        </div>
        </div>
                </div>
                
            </div>
            );

            
        }
    }


}

export default KurikulumPilihan;