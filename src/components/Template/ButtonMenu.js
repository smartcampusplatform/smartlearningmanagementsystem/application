import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Link } from 'react-router-dom';

class ButtonMenu extends React.Component{
    render(){
        return(
            <div className="col-4 text-center" >
                <Link to={this.props.link} className="btn btn-primary p-2" style={{minWidth: '150px' , height: '40px'}}>
                <FontAwesomeIcon  icon={this.props.icon} />
                <span className="ml-3">{this.props.name}</span></Link>
            </div>   
        );
    }
}

export default ButtonMenu;