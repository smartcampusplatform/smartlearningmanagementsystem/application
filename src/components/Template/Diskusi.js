import React from 'react';

class Diskusi extends React.Component{
    render(){
        return(
            <div className="container">
                <div className="card" style={{marginTop: '20px', marginBottom: '20px' }}>
                    <div className="card-body">
                        <p className="font-weight-bold card-text"> {this.props.konten} </p>
                    </div>
                    <div className="card-footer" >
                        <p> Favorit : {this.props.favorit}</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default Diskusi;