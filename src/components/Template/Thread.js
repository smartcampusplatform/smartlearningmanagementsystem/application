import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Link } from 'react-router-dom';

class Thread extends React.Component{
    render(){
        return(
            <div className="container">
                <div className="card" style={{marginTop: '10px', marginBottom: '50px' }}>
                    <div className="card-header">
                        <h5> <Link to={this.props.link} className="card-title">{this.props.judul} </Link> </h5>
                    </div>
                    <div className="card-body">
                        <p className="font-weight-bold card-text"> {this.props.konten} </p>
                    </div>
                    <div className=" card-footer">
                        <div className="row">
                            <div className="col-3">
                                <p> Favorit : {this.props.favorit}</p>
                            </div>
                            <div className="col-3">
                                <p> Status : {this.props.isdone}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Thread;